#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <errno.h>
#include <string.h>

#define mmap_size 1024//4194304 // 4MB
#define page_size 4096// 4KB
#define total_size mmap_size * mmap_size * mmap_size//* (iter_size / 2 - 1)
#define list_size total_size / 4096//iter_size
#define PAGEMAP_ENTRY 8
#define GET_BIT(X,Y) (X & ((uint64_t)1<<Y)) >> Y
#define GET_PFN(X) X & 0x7FFFFFFFFFFFFF
#define sample_size 500
#define iter_size 16

const int __endian_bit = 1;
#define is_bigendian() ( (*(char*)&__endian_bit) == 0 )

int conv_bit(uint64_t bit);
int opt;
int i, c, pid, status;
unsigned long virt_addr; 
uint64_t read_val, file_offset;
char path_buf [0x100] = {};
FILE * f;
char *end;
int excl_bin;
int col_bin;
char* free_ptr;
int test_pair_size;
void make_PFN();
typedef struct table_entry{
    uint64_t PA;
    uint64_t VA;
} table_entry;
typedef struct pair{
    table_entry first;
    table_entry second;
    uint64_t diff_bit;
    int latency;
} pair;
pair test_pair[sample_size];
table_entry PT[list_size];
inline uint64_t time_stamp();
inline void clflush();
void Flush_Cache(char *first_PA, char *second_PA);
int PTN;
int func();
int read_pagemap(char * path_buf, unsigned long virt_addr);
uint64_t Check_Gap(uint64_t a, uint64_t b);
int make_table();
void sort(int low, int high);
int partition (int low, int high);
void print();
void concheck();
int latency_check(char* a, char* b);
int test();
void select_();
void dec2bin(uint64_t a, char *bin);
int diff(uint64_t a, uint64_t b);
void test_push(int i,int j,int bit);
int test_size();
void diff_swap(int a, int b);
void PT_push(uint64_t PA, uint64_t VA);
void get_col(int a);
void _test_push(uint64_t PA, uint64_t PA_, uint64_t VA, uint64_t VA_);
int _exist();
void _select_(int num);
uint64_t row_bit;
void make_rest_col();
int find_bank_bit(int dif_num);
void init_test_pair();
void print_diff_bit(uint64_t diff_bit);
void find_row_bit(); // 5~12

uint64_t row_bit;
uint64_t col_bit;
uint64_t bank_bit[30];
int bank_size;

void push_row_bit(uint64_t bit);
void push_col_bit(uint64_t bit);
void push_bank_bit(uint64_t bit);
void find_R_nodes();
void find_C_nodes();
void find_B_nodes(int diff_num);
uint64_t V_R_C;
uint64_t V_R_C_;
void print_nodes(int opt);
int C(int n, int r);
long int fact(int x);

int main()
{

    int input;
    int i;
    table_entry table_entry[list_size];
    row_bit = 0;
    col_bit = 0;
    for(i=0;i<30;i++)
        bank_bit[i] = 0;
    bank_size = 0;
    init_test_pair();

    while(1)
    {
        printf("1. make page table (about PFN)\n2. Latency check\n3. check column bit\n");
        printf("4. check rest column bit\n5. exit\n-");
        scanf("%d",&input);
        switch (input){
            case 1:
                make_PFN();
                break;
            case 2:
                printf("V_R_C : %x",(unsigned int)V_R_C);
                find_bank_bit(2);
                break;
            case 5:
                return 0;
            case 3:
                get_col(23);//TODO input is one of the row bits
                break;

            case 4: 
                make_rest_col();
                break;
            case 7:
                find_R_nodes();
                break;
            case 8:
                find_C_nodes();
                break;
            case 9:
                find_B_nodes(2);
                break;

            default:
                printf("retype\n");
                break;
        }
    }

    return 0;
}

void push_row_bit(uint64_t bit)
{
    row_bit = row_bit|bit;
    return ;
}
void push_col_bit(uint64_t bit)
{
    col_bit = col_bit|bit;
    return ;
}
void find_R_nodes()
{
    int latency;
    int i;
    int j;
    V_R_C = 0b1111111111111111111000000000000; // this is 12~30 bit //TODO change
    int limit = _size(V_R_C)+ 8;

    while(1){
        make_PFN();
        latency = find_bank_bit(1);

        printf("average : %d\n",latency);
       printf("test size : %d\n",test_pair_size);
        if(test_pair_size >= limit)
            break;
   printf("test size : %d, limit : %d\n",test_pair_size, limit);

    }


    for(i=0;i<sample_size;i++)
        if(test_pair[i].diff_bit!=0)
        {
            j++;
            latency = latency + test_pair[i].latency;
        }


    latency = latency/j + 30;
    for(i=0; i<sample_size; i++)
    {
        if(test_pair[i].diff_bit!=0)
            if(test_pair[i].latency>latency )
                push_row_bit(test_pair[i].diff_bit);
    }

//    find_row_bit();
    printf("bit : %x\n",(unsigned int)row_bit);
    print_nodes(0);

    return ;
}
void find_C_nodes()
{
    V_R_C = V_R_C & ~row_bit;
    V_R_C = V_R_C & ~(0xFFF);
    int row;
    int row_index=0;
    int test_size = 100;
    uint64_t diff_bit;
    int latency = 0;
    int limit;
    uint64_t PA,PA_,VA,VA_;

    int i, j;

    row = row_bit;
    for(i =0 ; i < 64 ; i++) // pick one row bit
    {
        if(row & 1<<i != 0)
            break;
        row = row / 2;
    }
    row_index = i;

    printf("row index : %d\n",row_index);

    for(i=0; i<sample_size; i++)
    {
        if(test_pair[i].diff_bit == (1<<row_index))
        {
            PA = test_pair[i].first.PA;
            VA = test_pair[i].first.VA;
            PA_ = test_pair[i].second.PA;
            VA_ = test_pair[i].second.VA;
        }
    }
//    init_test_pair();

    test_pair_size = 0;
    for(i=0;i<sample_size;i++)
        test_pair[i].diff_bit = 0;

    for(j=4;j<12;j++)
        _test_push(PA, (PA_)|(1<<j), VA, VA_|(1<<j));
    ///////////////////////////////////

    limit = _size(V_R_C) + 8;
    while(1){
        printf("AAAAAAAAAAAAAAAA\n");
        make_PFN();
        printf("point : %p\n",free_ptr);
        for(i=0;i<test_size;i++)
            for(j=i+1;j<list_size;j++)
            {
                diff_bit = _diff((PT[i].PA)<<12,(PT[j].PA)<<12);
                if(_size(diff_bit) == 2)
                {
                    if(_size(diff_bit&V_R_C) == 1)
                    {
                        if(_size(diff_bit&(1<<row_index)) == 1)
                        {
                            if(!_exist(diff_bit))
                                _test_push((PT[i].PA)<<12,(PT[j].PA)<<12,PT[i].VA,PT[j].VA);
                        }
                    }
                }
            }
        printf("point : %p\n",free_ptr);

//        free(free_ptr);
        if(test_pair_size >= limit)
            break;

        printf("size : %d, limit : %d\n",test_pair_size, limit);
    }
    //////////////////////////////////

    for(i=0;i<sample_size;i++)
    {
        if(test_pair[i].diff_bit != 0)
            test_pair[i].latency = latency_check((char *)(test_pair[i].first.VA),(char *)(test_pair[i].second.VA));
    }

    j = 0;
//    sort(0, sample_size -1);

    for(i=0;i<sample_size;i++)
    {
        if((test_pair[i].diff_bit!=0))//&&(_size(test_pair[i].diff_bit) == 2))
        {
            printf("%d. ",++j);
            printf("first : %x, second : %x, ",(unsigned int)(test_pair[i].first.PA), (unsigned int)(test_pair[i].second.PA));
            print_diff_bit(test_pair[i].diff_bit);
            printf("latency: %d\n", test_pair[i].latency);
            latency = latency + test_pair[i].latency;
        }
    }
    latency = latency /j;

    printf("average latency : %d\n", latency);

    for(i=0;i<sample_size;i++)
        if(test_pair[i].latency>latency - 10)
            push_col_bit((test_pair[i].diff_bit)&~(1<<row_index));
    print_nodes(1);
}


void find_B_nodes(int diff_num)
{
    int test_size = 100;
    uint64_t diff_bit;
    int limit;

    
    V_R_C_ = 0xFFF;
    print_nodes(0);
    print_nodes(1);
    V_R_C = V_R_C & ~col_bit;
    V_R_C = V_R_C & ~(0xFFF);

    V_R_C_ = V_R_C_ & ~col_bit;
    V_R_C_ = V_R_C_ & ~row_bit;

    V_R_C = V_R_C|V_R_C_;
    V_R_C = V_R_C&(~(0xF));


    printf("V_R_C!!!! : ");
    print_diff_bit(V_R_C);
    printf("\n");





    //printf("V_R_C: %x, V_R_C_: %x",V_R_C,V_R_C_);


    int i,j,k;
    int latency = 0;
/*
/////////////////////////////////////////////////////////////
    make_PFN();
    for(k = diff_num-_size(V_R_C_);k<=diff_num;k++)
    {
        for(i=0;i<test_size;i++)
            for(j=i+1;j<list_size;j++)
            {
                diff_bit = _diff((PT[i].PA)<<12, (PT[j].PA)<<12);
                if(_size(diff_bit) == k)
                    if(_size(diff_bit&V_R_C) == k)
                        if(!_exist(diff_bit))
                            _test_push((PT[i].PA)<<12,(PT[j].PA)<<12,PT[i].VA,PT[j].VA);
            }
    }
////////////////////////////////////////////////////////////
    
    for(k= diff_num-_size(V_R_C_);k<diff_num;k++)
    {
        for(i=0;i<sample_size;i++)
        {
            if(_size(test_pair[i].diff_bit) == k)
                convert(k,i);
        }
    }
    */
    init_test_pair();

    ///////////////////////////////////////////////////
    printf("size : %d, diff_num : %d\n",_size(V_R_C),diff_num);

    limit = 0;
    V_R_C = V_R_C&~(0xFFF);
    limit = C(_size(V_R_C),diff_num);



    if(V_R_C_ != 0){
        while(1){
            printf("test_pair_size : %d, limit : %d\n",test_pair_size, limit);
//            make_table();
            make_PFN();
            for(i=0;i<test_size;i++)
                for(j=i+1;j<list_size;j++)
                {
                    diff_bit = _diff((PT[i].PA)<<12, (PT[j].PA)<<12);
                    if(_size(diff_bit) == diff_num)
                        if(_size(diff_bit&V_R_C)==diff_num)
                            if(!_exist(diff_bit))
                                _test_push((PT[i].PA)<<12,(PT[j].PA)<<12,PT[i].VA,PT[j].VA);
                }
            //       free(free_ptr);
            if(test_pair_size >= limit)
                break;

            for(i=0;i<sample_size;i++)
            {
                if((test_pair[i].diff_bit!=0))//&&(_size(test_pair[i].diff_bit) == 2))
                {
                    printf("first : %x, second : %x, ",(unsigned int)(test_pair[i].first.PA), (unsigned int)(test_pair[i].second.PA));
                    print_diff_bit(test_pair[i].diff_bit);
                    printf("latency: %d\n", test_pair[i].latency);
                }
            }

            free(free_ptr);


        }
    }
    else
    {
        while(1){
            printf("test_pair_size : %d, limit : %d\n",test_pair_size, limit);
            make_PFN();
            for(i=0;i<test_size;i++)
                for(j=i+1;j<list_size;j++)
                {
                    diff_bit = _diff((PT[i].PA)<<12, (PT[j].PA)<<12);
                    if(_size(diff_bit) == diff_num)
                        if(_size(diff_bit&V_R_C)==diff_num)
                            if(!_exist(diff_bit))
                                _test_push((PT[i].PA)<<12,(PT[j].PA)<<12,PT[i].VA,PT[j].VA);
                }
            //       free(free_ptr);
            if(test_pair_size >= limit)
                break;
            free(free_ptr);

        }

    }
    ///////////////////////////////////////////////////////

    for(i=0;i<sample_size;i++)
        if(test_pair[i].diff_bit != 0)
            test_pair[i].latency = latency_check((char *)(test_pair[i].first.VA), (char *)(test_pair[i].second.VA));
    j = 0;

    for(i=0;i<sample_size;i++)
        if((test_pair[i].diff_bit!=0))
        {
            printf("%d. ",++j);
            printf("first : %x, second : %x, ",(unsigned int)(test_pair[i].first.PA), (unsigned int)(test_pair[i].second.PA));
            print_diff_bit(test_pair[i].diff_bit);
            printf("latency %d\n", test_pair[i].latency);
            latency = latency + test_pair[i].latency;
        }
    latency = latency / j;

    for(i=0;i<sample_size;i++)
        if(test_pair[i].diff_bit != 0)
            if(test_pair[i].latency>latency+10)
                push_bank_bit(test_pair[i].diff_bit);
    print_nodes(2);

}
void push_bank_bit(uint64_t diff_bit)
{
    bank_bit[bank_size++] = diff_bit;
}
void convert(int size, int index)
{
    int i;
}
void print_nodes(int opt)
{
    int i,j;
    uint64_t bit_ = 0;
   
    if(opt == 0)
    {
        bit_ = row_bit;
        printf("row bit : ");
    }
    else if(opt == 1)
    {
        bit_ = col_bit;
        printf("column bit : ");
    }
    else if(opt == 2)
        printf("bank bit: ");

    if(opt == 2)
    {
        for(j=0;j<bank_size;j++){
            bit_ = bank_bit[j];
            for(i=0;i<64;i++){
                if(bit_&1 == 1)
                    printf("%d ",i);
                bit_=bit_/2;

            }
            printf("\n");
        }


    }
    else
    {
        for(i =0;i <64;i++)
        {
            if(bit_&1 == 1)
                printf("%d ",i);
            bit_ = bit_ / 2;
        }
        printf("\n");

    }
    return ;
}

int C(int n,int r)
{
    return (fact(n)/fact(n-r))/fact(r);
}
long int fact (int x)
{
    long int f = 1;
    int i ;
    for(i=1;i<=x;i++)
        f=f*i;
    return f;
}




void init_test_pair()//if diff_bit is 0, then it is initialized
{
    int i;
    test_pair_size =0;
    for(i < 0 ; i < sample_size ; i++ )
    {
        test_pair[i].diff_bit = 0;
        test_pair[i].latency = 0;
    }

}

void find_row_bit()
{
    int i;
    for(i=4; i<12; i++)
        if(!_exist(_diff((PT[0].PA)<<12, ((PT[0].PA)<<12)|(1<<i))))
            _test_push((PT[0].PA)<<12, ((PT[0].PA)<<12)|(1<<i), PT[0].VA, (PT[0].VA)|(1<<i));
    return;
}


int find_bank_bit(int dif_num) // find bank bit within >12 bit (PFN bit) // make PT and execute this, 2->8
{
//    int V_R_C; // binary bit which is not R and C bit
//    int V_R_C_ = 0b100; // bit between 5~11 / TODO change, this is 6
//    V_R_C = 0b11111111111000000000000; // this is 12~30 bit //TODO change

    int latency = 0;

    int i,j,m,n;
    uint64_t diff_bit;
    int test_size = 100; 
   // test_pair a[sample_size];

//    init_test_pair();
    //        get_col(23); // 23 is row bit not between ~11



    if(dif_num == 1)
        find_row_bit();

    for(i = 0 ; i < test_size ; i++) // make test_pair
    {
        for(j = i+1 ; j < list_size ; j++)
        {
            diff_bit = _diff((PT[i].PA)<<12,(PT[j].PA)<<12);
            if(_size(diff_bit) == dif_num) // originally # of diff_bit 2
            {
                if(_size(diff_bit&V_R_C) == dif_num) // diff_bit is within V_R_C
                {
                    if(!_exist(diff_bit))
                    {
                        _test_push((PT[i].PA)<<12,(PT[j].PA)<<12,PT[i].VA,PT[j].VA);
                    }

                }
            }
            if(test_pair_size == sample_size)
                break;

        }
        if(test_pair_size == sample_size)
            break;
    }

/*

    if(dif_num > _size(V_R_C_)) // how can i solve this problem with 5~11 bit
    {
        for(i = 0; i< test_size; i++)
        {
            for(j = i +1 ; j< list_size ; j++)
            {
                diff_bit = _diff((PT[i].PA)<<12,(PT[j].PA)<<12);
                if(_size(diff_bit) == dif_num-1)
                {
                    if(_size(diff_bit&V_R_C) == dif_num - 1)
                    {
                        if(!_exist(diff_bit))
                            _test_push((PT[i].PA)<<12,(PT[j].PA)<<12,PT[i].VA,PT[j].VA);
                    }
                }
            }
        }

        for(i=0;i<sample_size; i++)
        {
            if(dif_num - 1 == _size(test_pair[i].diff_bit))
            {
                if(_size(V_R_C_) == 1)
                {
                   _test_push(test_pair[i].first.PA,((test_pair[i].second.PA)|V_R_C_<<5),test_pair[i].first.VA,(test_pair[i].second.VA)|(V_R_C_<<5));
                }
                else // there are more than two different bit within 5 ~ 11
                {
                }
            }
        }
    }

   */
    printf("size : %d\n",_size(V_R_C_));

    /*
    if(dif_num > _size(V_R_C_))
    {
        for(i = 0; i<sample_size; i++)
        {
            for(j = i+1; j< list_size; j++)
            {
                diff_bit = diff((PT[i].PA)<<12,(PT[j].PA)<<12);
                if(_size(diff_bit) > 0 && _size(diff_bit) <dif_num)
                {
                    if(_size(diff_bit&V_R_C) == _size(diff_bit))
                    {
                        if(!_exist(diff_bit))
                            _test_push((PT[i].PA)<<12, (PT[j].PA)<<12, PT[i].VA, PT[j].VA);
                    }
                }
            }
        }
        for(j=0;j<sample_size;j++)
        {
            for(i = 1; i<V_R_C_+1; i++)
            {
                if((i & V_R_C_) == i)
                {
                    if(_size(test_pair[j].diff_bit) + _size(i) == dif_num )
                    {
                        _test_push((test_pair[j].first.PA)<<12 , ((test_pair[j].second.PA)<<12)|i, test_pair[j].first.VA , test_pair[j].second.VA);
                    }
                }
            }
        }


    }*/
    

    for(i = 0; i<sample_size ; i++)
    {
        if(test_pair[i].diff_bit != 0)
            test_pair[i].latency = latency_check((char *)(test_pair[i].first.VA),(char *)(test_pair[i].second.VA));
    }

 
    j = 0;
 //   sort(0,sample_size - 1);




    /***********************print*****************************/
    for(i = 0; i < sample_size ; i++)
    {
        if((test_pair[i].diff_bit != 0)&&(_size(test_pair[i].diff_bit) == dif_num))
        {
            printf("%d. ",++j);
            printf("first : %x, second : %x, ",(unsigned int)(test_pair[i].first.PA), (unsigned int)(test_pair[i].second.PA));
            
            print_diff_bit(test_pair[i].diff_bit);
            //printf("latency: %d\n", latency_check((char *)(test_pair[i].first.VA),(char *)(test_pair[i].second.VA)));
            printf("latency: %d\n",test_pair[i].latency);
            latency = latency + test_pair[i].latency;
        }
    }
    printf("%d. ",j + 1);
    printf("average latency : %d\n", latency/j);

    return latency/j;


}


int * split(int a)
{

}


void print_diff_bit(uint64_t diff_bit)// only PFN!!
{
    int i, j;
    int rand;
    uint64_t bit;
    bit = diff_bit;
    printf("diff bit: [ ");
    for(i = 0; i < 64 ; i++)
    {
        if(((bit / 2)<<1) != bit)
            printf("%d ",i);
        bit = bit/2;
        if(bit == 0)
            break;
    }
    printf("]");
}


void make_rest_col()// two bit compare row bit and >11 bit / one row bit & rest bit in order to find column bit
{
    int d;
    int index = 0;
    row_bit = 0;
    int size=0;
    uint64_t row[32] = {0};
    uint64_t rest[32] = {0};
    int i,j,k;


/*    printf("type row bit\n");
    for(index =0 ;index<32; index++)
    {
        printf("-");
        scanf("%d",&d);
        printf("\n");
        if(d == 0)
            break;
        row[index] = (1<<d);
        size++;
    }

    printf("type rest bin\n");
    for(index =0 ;index<32; index++)
    {
        printf("-");
        scanf("%d",&d);
        printf("\n");
        if(d == 0)
            break;
        rest[index] = (1<<d);
        size++;
    }*/
    k = 0;
   row[0] = 23; // TODO change
   
   rest[0] = 12; // TODO change
   rest[1] = 13;
   rest[2] = 14;
   rest[3] = 15;
   rest[4] = 16;
   rest[5] = 17;
   rest[6] = 18;
   rest[7] = 19;
   rest[8] = 20;
   rest[9] = 21;
   rest[10] = 22;


    for(i=0;i<20;i++)
    {
        for(j=0;j<list_size;j++)
        {

            if(_diff(PT[i].PA,PT[j].PA) == ((1<<(row[0]-11))|(1<<(rest[k]-11)))) // TODO
            {
                printf("%d.",k);
                printf("[%d , %d]",(int)row[0],(int)rest[k]);
                printf("first : %x, second : %x, ",(unsigned int) PT[i].PA, (unsigned int) PT[j].PA);
                printf("latency : %d\n",latency_check((char *)PT[i].VA, (char *)PT[j].VA));

                k++;
            }
        }
    }




    return ;
}


void get_col(int a) // 2 #, column, bit 5~11, input bit > 12 through scanf and check latency with bit 5~11
{
    //int a;
    //scanf("%d",&a); // scanf one row bit in order to compare latency
    int i,j,k;
    uint64_t first, second;
    int diff_bit;
    k = 5;
    for(i = 0; i<sample_size; i++)
    {
       // printf("first : %x, second : %x\n",1<<(a-11),test_pair[i].diff_bit);
/*****        if(1<<(a-11) == (test_pair[i].diff_bit))*****/
        if(1<<a == (test_pair[i].diff_bit))
        {
            for(j=5;j<12;j++)
            {
                first = test_pair[i].first.VA;
                second = ((test_pair[i].second.VA) | (1<<j));
                printf("different bit: %d, ",j);
                printf("first : %x, second : %x, changed second : %x, ",(unsigned int) first, (unsigned int)(test_pair[i].second.VA),(unsigned int) second);
                printf("first PA : %x, second PA : %x, ",(unsigned int)(test_pair[i].first.PA),(unsigned int)(test_pair[i].second.PA));
                printf("latency : %d\n",latency_check((char *)first, (char *)second));
            }
        }
    }
}

int test()
{
    int i,j;
    int start,end;

    init_test_pair();
    //    select_();
    _select_(1);
    char a,b;
    char *c;
    char *d;
    //    int size = test_size();
    int k = 0;


    printf("start!!!!!!\n");
    printf("size : %d\n",test_pair_size);
    //    test_size();
    for(i = 0 ; i < sample_size ; i++)
    {
        if(test_pair[i].diff_bit != 0)
        {
            test_pair[i].latency = latency_check((char *)(test_pair[i].first.VA),(char *)(test_pair[i].second.VA));
        }
    }
  
//    sort(0, sample_size - 1);

    for(i = 0; i<sample_size; i++)
    {

        if(test_pair[i].diff_bit != 0)
        {
/***            if(opt == 0) printf("%d. different bit: %d,  latency: %d,",k,conv_bit(test_pair[i].diff_bit)-1,test_pair[i].latency);
            if(opt == 1) printf("%d. different bit: %d,  latency: %d,",k,conv_bit(test_pair[i].diff_bit)+11,test_pair[i].latency);***/
            printf("%d. different bit: %d,  latency: %d,",k,conv_bit(test_pair[i].diff_bit)-1,test_pair[i].latency);
            printf("  first: %x , second %x\n",(unsigned int)(test_pair[i].first.PA), (unsigned int)(test_pair[i].second.PA));
            k++;
        }

    }
   // free(free_ptr);
    return (start-end)/1000;
}
int latency_check(char *a, char *b)
{
    int j;
    int start, end;
    start = time_stamp();
    for(j=0;j<10000;j++)
    {
        asm volatile
            (
             "mov (%0), %%r8;"
             "mov (%1), %%r9;"
             "clflush (%0);"
             "clflush (%1);"
             "mfence;"
             :: "q"(a), "p"(b)
            );
    }
    end = time_stamp();
    return (end-start)/10000;
}

void select_() // make test_pair
{
    int i,j,k = 0;
    int bit;

    for(i = 0; i< 20; i++)
    {
        for(j=i+1; j<list_size; j++)
        {
            bit = diff(PT[i].PA,PT[j].PA);
            if((bit != -1))// && !(test_exist(bit)))//differnet only one bit
            {
                test_push(i,j,bit);
                break;
            }
            if(j == list_size - 1)//not found
            {
                break;
            }
        }
        //        if(test_size() == sample_size)
        if(test_pair_size == sample_size)
            break;
    }
//    sort(0, sample_size-1);
    return ;
}

void _select_(int num)
{
    int i,j;
    int limit = 5;
    int diff_bit = 0;
    int k = 0;
    for(i=0;i<20;i++)
    {
        for(j=i;j<list_size;j++)
        {
            diff_bit = _diff(PT[i].PA,PT[j].PA);
            if((_size(diff_bit) == num))//&&!(_exist(diff_bit)))
            {
                if(_exist(diff_bit))
                    ;
                else
                {

                    _test_push(PT[i].PA,PT[j].PA,PT[i].VA,PT[j].VA);            
                    k++;
                    break;
                }
            }
        }
    }
//    sort(0, sample_size -1);
    return ;
}
void _test_push(uint64_t PA, uint64_t PA_, uint64_t VA, uint64_t VA_)
{
    int size = test_pair_size;
    int diff_bit = _diff(PA,PA_);
    //    if(_size(diff_bit) == num)
    //        if(!(_exist(diff_bit)))
    //        {  
    test_pair[size].first.VA = VA;
    test_pair[size].first.PA = PA;
    test_pair[size].second.VA = VA_;
    test_pair[size].second.PA = PA_;
    test_pair[size].diff_bit = diff_bit;
    test_pair_size++;
    //        }
    return ;
}
int _diff(uint64_t PA, uint64_t PA_)
{
    return PA^PA_;
}
int _size(uint64_t bin)
{
    char buffer[64] = {0};
    int i;
    int _size = 0;

    int remainder;
    while(bin != 0)
    {
        remainder = bin%2;
        bin = bin/2;
        _size = _size + remainder;
    }
    return _size;
}
int _exist(uint64_t diff_bit)
{
    int i;
    for(i = 0;i <sample_size; i++)
    {
        if(test_pair[i].diff_bit == diff_bit)
        {
            return 1;
        }
    }
    return 0;
}
int print_bin(int dec)
{
    while(1)
    {

    }
}




int test_exist(int bit) // if there exists a same bit in test_pair list, then return 1;
{
    int i;
    for(i = 0; i < test_pair_size ; i++)
    {
        if(test_pair[i].diff_bit == bit)
            return 1;
    }
    return 0;

}
void test_push(int i, int j, int bit)
{
    int size = test_pair_size;
    test_pair[size].first.VA = PT[i].VA;
    test_pair[size].first.PA = PT[i].PA;
    test_pair[size].second.VA = PT[j].VA;
    test_pair[size].second.PA = PT[j].PA;
    test_pair[size].diff_bit = bit;
    test_pair_size++;
    return ;
}
int diff(uint64_t a, uint64_t b)//only one different bit / return different bit
{
    char buffer_a[64] = {'0'};
    char buffer_b[64] = {'0'};
    int i;
    int dif = 0;
    dec2bin( a, buffer_a);
    dec2bin( b, buffer_b);

    for(i = 0; i < 64; i++)
        //    for(i=64 ; i>0 ; i--)
    {
        if(buffer_a[i] == buffer_b[i])
            ;
        else
            dif++;
    }
    if(dif == 1)
    {
        for(i = 0; i < 64; i ++)
            //        for(i=64 ; i>0 ; i--)
            if(buffer_a[i] != buffer_b[i])
                break;
        return i-1;
    }
    else
        return -1;
}




void dec2bin(uint64_t a, char *bin)
{
    int quot = a;
    int i = 1;

    while (quot != 0)
    {
        bin[i++] = quot % 2 + 48;
        quot = quot / 2;
    }

    return ;
}
int max(int * size)
{
    int i,j = 0 ;
    int ret = 0;

    for(i = 0; i<list_size; i++)
    {
        if(j < size[i])
        {
            j = size[i];
            ret = i;
        }
    }
    return ret;
}
void sort(int low, int high) // sort test pair with latency
{
    if(low < high)
    {
        int pi = partition (low, high);

        sort(low, pi-1);
        sort(pi+1, high);
    }
}
int partition (int low, int high)
{
    int pivot = test_pair[high].latency;
    int i = (low - 1);
    int j = low;

    for (j; j <= high - 1; j++)
    {
        if (test_pair[j].latency <= pivot)
        {
            i++;
            diff_swap(i, j);
        }
    }
    diff_swap(i+1, high);
    return i+1;
}
void print()
{
    int i = 0;
    for(i = 0; i < list_size; i++)
    {
        printf("%d. virtual address: %x  /  page frame number: %x\n", i, (unsigned int)PT[i].VA, (unsigned int)PT[i].PA);
    }
    return ;
}
void diff_swap(int a, int b)
{
    pair swap_pair;
    swap_pair.first.VA = test_pair[a].first.VA;
    swap_pair.first.PA = test_pair[a].first.PA;
    swap_pair.second.VA = test_pair[a].second.VA;
    swap_pair.second.PA = test_pair[a].second.PA;
    swap_pair.diff_bit = test_pair[a].diff_bit;
    swap_pair.latency = test_pair[a].latency;

    test_pair[a].first.VA = test_pair[b].first.VA;
    test_pair[a].first.PA = test_pair[b].first.PA;
    test_pair[a].second.VA = test_pair[b].second.VA;
    test_pair[a].second.PA = test_pair[b].second.PA;
    test_pair[a].diff_bit = test_pair[b].diff_bit;
    test_pair[a].latency = test_pair[b].latency;

    test_pair[b].first.VA = swap_pair.first.VA;
    test_pair[b].first.PA = swap_pair.first.PA;
    test_pair[b].second.VA = swap_pair.second.VA;
    test_pair[b].second.PA = swap_pair.second.PA;
    test_pair[b].diff_bit = swap_pair.diff_bit;
    test_pair[b].latency = swap_pair.latency;
}

int make_table()
{
    char *first_PA, *second_PA ;
    uint64_t gap = 0;
    int i,j,k = 0;
    char test;

    int PA = 0;
    PTN = 0;
    opt = 0;
    int total_size_ = mmap_size*mmap_size;
    first_PA = (char *)malloc(total_size);//virtual address TODO
    second_PA = first_PA;
    free_ptr = first_PA;


    pid = getpid();
    printf("pid : %d\n",pid);
    sprintf(path_buf, "/proc/%u/pagemap", pid);
    for(i=0;i<total_size; i++)
        memset(second_PA++, '1', 1);


    for(i=0; i<total_size_; i = i + iter_size)
    {
        read_pagemap(path_buf, (unsigned long)first_PA);
        first_PA = first_PA + iter_size;
    }
    printf("END\n");
    return 0;


}
void make_PFN()
{
    char *first_PA, *second_PA ;
    uint64_t gap = 0;
    int i,j,k = 0;
    char test;
    opt = 1;
    uint64_t mask = 0xFFFFFFFFFFFFF000;

    int PA = 0;
    PTN = 0;
    first_PA = (char *)malloc(total_size);//virtual address TODO
    second_PA = first_PA;
    free_ptr = first_PA;
   

    pid = getpid();
    printf("pid : %d\n",pid);
    sprintf(path_buf, "/proc/%u/pagemap", pid);
    for(i=0;i<total_size; i++)
        memset(second_PA++, '1', 1);

    for(i=0;i<total_size; i = i + page_size)
    {
        read_pagemap(path_buf, ((unsigned long)first_PA));//&mask);
        first_PA = first_PA + page_size;
    }
    printf("END\n");
    return ;
}

inline uint64_t time_stamp()
{
    unsigned hi, lo;
    unsigned hi_, lo_;

    asm volatile (
            "cpuid;"
            "rdtsc;"
            : "=a"(lo), "=d"(hi)
            );
    return ((unsigned long long)lo)|(((unsigned long long)hi)<<32);
}
inline void clflush(volatile void *p)
{
    asm volatile ("clflush (%0)" :: "r"(p));
}

int read_pagemap(char * path_buf, unsigned long virt_addr)
{
    //printf("Big endian? %d\n", is_bigendian());
    f = fopen(path_buf, "rb");
    if(!f){
        printf("Error! Cannot open %s\n", path_buf);
        sleep(1);
        return -1;
    }

    //Shifting by virt-addr-offset number of bytes
    //and multiplying by the size of an address (the size of an entry in pagemap file)
    file_offset = virt_addr / getpagesize() * PAGEMAP_ENTRY;
    status = fseek(f, file_offset, SEEK_SET);
    if(status){
        perror("Failed to do fseek!");
        return -1;
    }
    errno = 0;
    read_val = 0;
    unsigned char c_buf[PAGEMAP_ENTRY];
    for(i=0; i < PAGEMAP_ENTRY; i++){
        c = getc(f);
        if(c==EOF){
            printf("\nReached end of the file\n");
            return 0;
        }
        if(is_bigendian())
            c_buf[i] = c;
        else
            c_buf[PAGEMAP_ENTRY - i - 1] = c;
        //printf("[%d]0x%x ", i, c);
    }
    for(i=0; i < PAGEMAP_ENTRY; i++){
        //printf("%d ",c_buf[i]);
        read_val = (read_val << 8) + c_buf[i];
    }
    if(GET_BIT(read_val, 63))
    {
        if(opt == 1)
        {PT[PTN].PA = GET_PFN(read_val);
            PT[PTN++].VA = (unsigned long) virt_addr;}
        if(opt == 0)
            PT_push(((GET_PFN(read_val)<<12)|(0xFFF&virt_addr)),((unsigned long) virt_addr));
    }
    //return GET_PFN(read_val);
    else
        printf("Page not present\n");
    if(GET_BIT(read_val, 62))
        printf("Page swapped\n");
    fclose(f);
    return 0;
}
void PT_push(uint64_t PA, uint64_t VA)
{
    PT[PTN].PA = PA;
    PT[PTN].VA = VA;
    PTN++;
}
int conv_bit(uint64_t bit)
{
    char buf[64] = {0};
    dec2bin(bit, buf);
    int i;
//    printf("int bit : %"PRIu64", string bit :",bit);
    for(i =0; i<64; i++)
    {
        if(buf[i] == '1')
            return i;
//        printf("%c",buf[i]);
    }
//    printf("\n");
    return i;
}
