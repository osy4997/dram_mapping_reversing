#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <inttypes.h>

typedef struct ar{
    uint64_t bit[2048];
    int num;
} ar_;
typedef struct br{
    uint64_t bit[2048];
    int num;
} br_;
typedef struct pot_bit{
    uint64_t bit[2048];
    uint64_t Rbit[2048];
    int num;
} pot_bit_;

ar_ ar;
br_ br;
pot_bit_ pot_bit;
uint64_t pot_bank_bit;
typedef struct orig_bit_{
    int bit[64];
    int num;
}orig_bit_;

void pot_bit_push(uint64_t bit){
    pot_bit.bit[pot_bit.num++] = bit;
    return ;
};
int bit_size(uint64_t bit){
    int size = 0;
    int remainder;
    uint64_t bin = bit;
    while(bin){
        remainder = bin%2;
        bin = bin/2;
        size = size + remainder;
    }
    return size;
};
orig_bit_ orig_bit;

void init_orig_bit(){
    int i;
    for(i=0; i<64; i++)
        orig_bit.bit[i] = 0;
    orig_bit.num = 0;
    return ;
};
void push_orig_bit(int orig){
    orig_bit.bit[orig_bit.num++] = orig;
    return ;
};

void init_pot_bit(){
    int i, j, a, b, c, d = 0;
    uint64_t bit_;
    for(i=0; i<2048; i++){
        pot_bit.Rbit[i] = 0;
    }


    b = 0;
    a = 0;
    printf("a : %d\n", a);
    bit_ = pot_bank_bit;
    while(bit_){
        if(bit_&1 == 1){
            for(i=0; i<pot_bit.num; i++){
                c = (pot_bit.bit[i] & (1<<a)) >> a ;
                pot_bit.Rbit[i] = pot_bit.Rbit[i] | (c << b);

            }
            push_orig_bit(a);
            b++;

        }
        bit_ = bit_ >> 1;
        a++;
    }

    return ;
};
//uint64_t Rbank_bit[20];
/*void init_Rbank_bit(){
    int i;
    for(i=0; i<20; i++)
        Rbank_bit[i] = 0;
    return ;
};*/

void init_ar(){

    int i;
    for(i=0; i<2048; i++)
        ar.bit[i] = 0;

    for(i=0; i<(1<<bit_size(pot_bank_bit)); i++){
        ar.bit[i] = i;
    }
    ar.num = 1<<bit_size(pot_bank_bit);
    return ;

};

uint64_t answer[100];
int answer_num ;

void init_answer(){
    int i;
    for(i=0; i<100; i++)
        answer[i] = 0;
    answer_num = 0;
    return ;
};
void push_answer(uint64_t bit){
    answer[answer_num++] = bit;
    return ;
};


int exist_in(uint64_t bit){
    int i;

    for(i=0; i<answer_num; i++){
        if(answer[i] == bit){
            return 1;
        }
    }

    return 0;
};

uint64_t num_xor(int i, int size){
    uint64_t ret = 0;
    uint64_t j;
    int num = i;
    if(i == 0)
        return 0;
    else{
//        printf(" j. %"PRIx64", bit size : %d\n", j, bit_size(pot_bank_bit));
       for(j=0 ; j<size; j++){
           ret = ret ^ (pot_bit.Rbit[j] * (num&1));
           num = num >> 1;
       }
        
    }
    return ret;
};
void extend_pot_bit(){
    int i;
    for(i=0; i<2048; i++)
        pot_bit.bit[i] = 0;
    int num = pot_bit.num;
    pot_bit.num = 0;

    uint64_t bit_;
    for(i=0; i< (1<<num); i++){
        bit_ = num_xor(i, num);
//        printf("bit %"PRIx64"\n", bit_);
        if(!exist_in(bit_)){
            pot_bit_push(bit_);
//            printf("num : %d\n", pot_bit.num);
        }
    }

    return ;
};


void get_answer(){
    int i, j;
    uint64_t bit, bit_;
    init_answer();
    int T;
//    printf("1. %d, 2. %d\n", ar.num, pot_bit.num);
    for(i=0; i<ar.num; i++){
//        printf("i : %d\n", i);
//        bit = pot_bit.Rbit[0] & ar.bit[i];
        T = 0;
        for(j=0; j<pot_bit.num; j++){
            bit_ = pot_bit.Rbit[j] & ar.bit[i];
//            printf("ar : %"PRIx64", pot : %"PRIx64", pot_ : %"PRIx64", ", ar.bit[i], pot_bit.bit[0], pot_bit.bit[1]);
//            printf("first bit : %"PRIx64", second bit : %"PRIx64", ", bit, bit_);
//            printf("first bit size : %d, second bit size : %d\n", bit_size(bit)%2, bit_size(bit_)%2);
            //if((bit_size(bit)%2) != (bit_size(bit_)%2))
            if((bit_size(bit_))%2 != 0)
            {
                T = 1;
                break;
            }
        }
        if(T == 0)
            push_answer(ar.bit[i]);
//        printf("answer num : %d\n", answer_num);
    }


    //convert answer into all of the cases



    return ;
};

void print_answer(uint64_t bit){
    int i = 0;
    printf("[ ");
    uint64_t bit_ = bit;
    while(bit_){
        if(bit_&1 == 1){
            printf("%d ", orig_bit.bit[i]);
        }
        bit_ = bit_ >> 1;
        i++;
    }
    printf("]");
};

int main()
{
    int i, j, k, size = 0;
    pot_bank_bit = 0;
    init_pot_bit();
//    init_Rbank_bit();
    

/*    pot_bit.bit[0] = (1<<8)|(1<<18)|(1<<22);
    pot_bit.bit[1] = (1<<9)|(1<<18)|(1<<22);
    pot_bit.bit[2] = (1<<12)|(1<<18)|(1<<22);
    pot_bit.bit[3] = (1<<13)|(1<<18)|(1<<22);
    pot_bit.bit[4] = (1<<8)|(1<<15)|(1<<19);
    pot_bit.bit[5] = (1<<9)|(1<<15)|(1<<19);
    pot_bit.bit[6] = (1<<12)|(1<<15)|(1<<19);
    pot_bit.bit[7] = (1<<13)|(1<<15)|(1<<19);
    pot_bit.num = 8;*/




    pot_bit.num = 2;
    pot_bit.bit[0] = (1<<2)|(1<<4);
    pot_bit.bit[1] = (1<<1)|(1<<5);

    for(i=0; i<pot_bit.num; i++){
        pot_bank_bit = (pot_bank_bit | pot_bit.bit[i]);
    }

    
//    pot_bank_bit = 0b10110;
   
    
    for(i=0; i<pot_bit.num; i++)
        printf("bank bit : %"PRIx64"\n", pot_bit.bit[i]);




    init_pot_bit();


    extend_pot_bit();
   
    printf("end exted pot bit\n");
    printf("pot bit num : %d\n", pot_bit.num);
    for(i=0; i<pot_bit.num; i++)
        printf("pot bit : %"PRIx64"\n", pot_bit.bit[i]);
    printf("end print \n\n\n");
    init_ar();
    get_answer();

//    for(i=0; i<pot_bit.num; i++)
//        printf("bank bit : %"PRIx64"\n", pot_bit.Rbit[i]);
//    for(i=0; i<orig_bit.num; i++)
//        printf("orig : %d\n", orig_bit.bit[i]);

//    printf("\n\n\n");

     
    for(i=0; i<answer_num; i++)
        printf("answer : %"PRIx64"\n", answer[i]);


    for(i=0; i<answer_num; i++){
        print_answer(answer[i]);
        printf("\n");
    }


    return ;
}
